<?php
namespace Nomad\Cache;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Driver
 *
 * @package Nomad\Cache
 */
class Driver extends Core\BaseClass
{
    /**
     * @return $this
     */
    public function initialize()
    {
        return $this;
    }
}
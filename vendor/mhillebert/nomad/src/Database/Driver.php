<?php
namespace Nomad\Database;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Driver
 *
 * @package Nomad\Database
 */
class Driver extends Core\BaseClass
{
    /**
     * @return $this
     */
    public function initialize()
    {
        return $this;
    }
}
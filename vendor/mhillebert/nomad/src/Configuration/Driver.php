<?php
namespace Nomad\Configuration;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Driver class for configuration
 *
 * @package Nomad\Configuration
 */
class Driver extends Core\BaseClass
{
    /**
     * @var array //@TODO ??!@ whats this
     */
    protected $_parsed = array();

    /**
     * @return $this
     */
    public function initialize()
    {
        return $this;
    }
}
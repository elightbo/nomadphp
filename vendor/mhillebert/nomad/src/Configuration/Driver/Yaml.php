<?php
namespace Nomad\Configuration\Driver;

use Nomad\Configuration as Configuration;
use Nomad\Unify\ArrayMethods as ArrayMethods;
use Nomad\Exception as Exception;
use Spyc\Yaml as Spyc;

/**
 * Class Yaml
 * Driver for ini based configuration files
 *
 * @package Nomad\Configuration\Driver
 */
class Yaml
    extends Configuration\Driver
{
    /**
     * Parse a yaml file using spyc
     *
     * @param $path
     * @throws \Nomad\Exception\Argument
     * @throws \Nomad\Exception\Syntax
     * @return \stdClass
     */
    public function parse($path)
    {
        if (!$path) {
            throw new Exception\Argument("Cannot parse an empty path.");
        }
        $filename = substr($path, -4, 4) == '.yml' ? $path : $path . '.yml';
        if (!file_exists($filename)) {
            throw new Exception\Syntax("Cannot find '{$filename}' to parse.");
        }
        $array = \Spyc::YAMLLoad($filename);
        return ArrayMethods::toObject($array);
    }
}
<?php
namespace Nomad\Core;

/**
 * Interfacer MapperInterface
 * @package Nomad\Core
 */
interface MapperInterface
{
    public function mapFromDataSource(array $dataSourceArray);

    public function mapToDataSource(array $columnNames);
}
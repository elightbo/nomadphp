<?php
namespace Nomad\Core;

use Nomad\Unify as Unify;

/**
 * Class Inspector
 *
 * @package Nomad\Core
 */
class Inspector
{
    /**
     * @var object Current class for inspection
     */
    protected $_class;

    /**
     * @var array Holds inspected info
     */
    protected $_meta = array(
        'class' => array(),
        'properties' => array(),
        'methods' => array(),
    );

    /**
     * @var array
     */
    protected $_properties;

    /**
     * @var array
     */
    protected $_methods;

    /**
     * @var \ReflectionClass
     */
    private $_reflectionClass;

    private $_canUseCache;

    private $_doUseCache = false;

    private $_doClearCache = false;

    private $_pulledFromCache;


    /**
     * @param $class object
     */
    public function __construct($class)
    {
        $this->_class = get_class($class);
        if ($this->_doUseCache && ($memcached = Registry::get('memcached')) && !$this->_doClearCache) {
            $this->_canUseCache = true;
            $allMeta = unserialize($memcached->get('meta'));
            if (isset($allMeta[$this->_class])) {
                $this->_meta = $allMeta[$this->_class];
                $this->_pulledFromCache = true;
            }
        } else {
            $this->_pulledFromCache = false;
        }
    }

    /**
     * @return string
     */
    protected function _getClassComment()
    {
        $reflection = isset($this->_reflectionClass) ? $this->_reflectionClass : new \ReflectionClass($this->_class);
        return $reflection->getDocComment();
    }

    /**
     * @return \ReflectionProperty[]
     */
    protected function _getClassProperties()
    {
        $reflection = isset($this->_reflectionClass) ? $this->_reflectionClass : new \ReflectionClass($this->_class);
        return $reflection->getProperties();
    }

    /**
     * @return \ReflectionMethod[]
     */
    protected function _getClassMethods()
    {
        $reflection = isset($this->_reflectionClass) ? $this->_reflectionClass : new \ReflectionClass($this->_class);
        return $reflection->getMethods();
    }

    /**
     * @param $property
     * @return string
     */
    protected function _getPropertyComment($property)
    {
        if (isset($this->_class->$property)) {
            $reflection = new \ReflectionProperty($this->_class, $property);
            return $reflection->getDocComment();
        }
        return null;
    }

    /**
     * @param $method
     * @return string
     */
    protected function _getMethodComment($method)
    {
        $reflection = new \ReflectionMethod($this->_class, $method);
        return $reflection->getDocComment();
    }

    /**
     * @param $comment
     * @return array
     */
    protected function _parse($comment)
    {
        $meta = array();

        $pattern = "(@[a-zA-Z\\\\]+\s*[a-zA-Z0-9:|+*{}^$?<>=., \[\]\-()_\\\\]*)";
        $matches = Unify\StringMethods::match($comment, $pattern);

        if ($matches != NULL) {
            foreach ($matches as $match) {
                $parts = Unify\ArrayMethods::clean(
                    Unify\ArrayMethods::trim(Unify\StringMethods::split($match, "[\s]", 2))
                );
                $meta[$parts[0]] = true;
                if (count($parts) > 1) {
                    $meta[$parts[0]] = Unify\ArrayMethods::clean(
                        Unify\ArrayMethods::trim(Unify\StringMethods::split($parts[1], ","))
                    );
                }
            }
        }

        return $meta;
    }

    private function _cacheMe()
    { //change name to cacheEnabled
        if ($this->_canUseCache && $this->_doUseCache) {
            $cache = Registry::get('memcached');
            $allMeta = unserialize($cache->get('meta'));

            $allMeta[$this->_class] = $this->_meta;
            $cache->set('meta', serialize($allMeta));
        }
    }

    /**
     * @return array
     */
    public function getClassProperties()
    {
        if (!isset($this->_properties)) {
            $properties = $this->_getClassProperties();
            foreach ($properties as $property) {
                $_properties[] = $property->getName();
            }
        }
        return $this->_properties;
    }

    /**
     * @return array
     */
    public function getClassMethods()
    {
        if (!isset($this->_methods)) {
            $methods = $this->_getClassMethods();
            foreach ($methods as $method) {
                $name = $method->getName();
                if ($method->isPublic() && !$method->isConstructor() && !in_array($name, ['__call', '__get', '__set', '__construct', '__destruct'])) {
                    $params = $method->getParameters();
                    $paramNames = array();
                    foreach ($params as $param) {
                        $paramNames[] = $param->getName();
                    }
                    $this->_methods[$name] = $paramNames;
                } else {
                    $this->_methods[$name] = "PRIVATE";
                }

            }
        }
//$this->_cacheMe();
        return $this->_methods;
    }

    /**
     * Gets class annotations
     * @return array|null
     */
    public function getClassMeta()
    {
        if (empty($this->_meta['class'])) {
            $comment = $this->_getClassComment();
            if (!empty($comment)) {
                $this->_meta['class'] = $this->_parse($comment);
            } else {
                $this->_meta['class'] = null;
            }
        }
        //$this->_cacheMe();
        return $this->_meta['class'];
    }

    /**
     * Gets property annotations
     * @param $property
     * @return mixed
     */
    public function getPropertyMeta($property)
    {
        if (!isset($this->_meta['properties'][$property])) {

            $comment = $this->_getPropertyComment($property);
            if (!empty($comment)) {
                $this->_meta['properties'][$property] = $this->_parse($comment);
            } else {
                $this->_meta['properties'][$property] = NULL;
            }
        }
//        $this->_cacheMe();
        return $this->_meta['properties'][$property];
    }

    /**
     * Gets method annotations
     * @param $method
     * @return mixed
     */
    public function getMethodMeta($method)
    {
        if (!isset($this->_meta['methods'][$method])) {
            $comment = $this->_getMethodComment($method);
            if (!empty($comment)) {
                $this->_meta['methods'][$method] = $this->_parse($comment);
            } else {
                $this->_meta['methods'][$method] = NULL;
            }
        }
        //$this->_cacheMe();
        return $this->_meta['methods'][$method];
    }
}
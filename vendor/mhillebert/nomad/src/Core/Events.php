<?php
namespace Nomad\Core;
/**
 * Class Events
 *
 * @TODO    Currently not in use.
 * @package Nomad\Core
 */
class Events
    extends BaseClass
{
    protected $_eventsAvailable = array();

    protected $_preAct;

    protected $_reAct;

    public function addEvent($class, $method)
    {
        $fullClassAndMethodName = $class . "::" . $method;
        if (!in_array($fullClassAndMethodName, $this->_eventsAvailable)) {
            $this->_eventsAvailable[] = $fullClassAndMethodName;
        }
    }

    public function getEventsAvailable()
    {
        return $this->_eventsAvailable;
    }

    public function preAct($event, $action)
    {
        $this->_preAct[$event][] = $action;
    }
}
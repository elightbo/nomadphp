<?php
namespace Nomad\Core;
/**
 * Class Package
 * Class holder for standard package information.
 *
 * @package Nomad\Core
 */
class Package
{
    /**
     * Names of the directories for the appropriate classes
     *
     * @var array
     */
    protected static $_standardPackageDirs = array(
        'Controller',
        'Model',
        'Service',
    );

    /**
     * @return array
     */
    static public function getStandardPackageDirs()
    {
        return self::$_standardPackageDirs;
    }
}
<?php
namespace Nomad\Core;
/**
 * Class Request
 * Holds all the relevant information of the request
 *
 * @package Nomad\Core\
 */
class Request extends BaseClass
{
    /**
     * @var string Raw url
     */
    protected $_rawUrl;

    //@TODO add more $_SERVER[] stuff here
    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        //set the php server request vars here

        $options['inspector'] = false;

        parent::__construct($options);
    }

    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

}
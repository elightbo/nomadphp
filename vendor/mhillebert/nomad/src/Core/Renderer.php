<?php
namespace Nomad\Core;

use Nomad\Router\Route\Simple;
use Nomad\Unify\StringMethods;

/**
 * Class Renderer
 * Renders a dispatched instanced object.
 *
 * @package Nomad\Core
 */
class Renderer
{
    /**
     * @var string Theme name
     */
    protected $_theme;

    /**
     * @var array Actions to run to provide code for theme. From theme configuration.
     */
    protected $_blocks = array();

    protected $_css = array();

    protected $_js = array();

    /**
     * @var Object The instance we're working on.
     * @TODO abstract the $_view from controller so other services can have a view. Right now must be a controller
     */
    protected $_instance;

    /**
     * @param $instance
     */
    public function __construct($instance)
    {
        $this->_instance = $instance;
        if ($this->_instance->view) {
            $this->_theme = $this->_instance->view->getTheme();
            if (!$this->_theme) {
                //try to see if it was set in the kernel.yml, if not try to use default
				$namespace = $this->_instance->getRoute()->getNamespace();
				$namespaceParts = explode('\\', $namespace);
                $namespaceTopLevel = array_shift($namespaceParts);
                $kernalSetup = property_exists(Registry::get('packages'), $namespaceTopLevel) ? Registry::get('packages')->$namespaceTopLevel : '';
                if ($kernalSetup && property_exists($kernalSetup, 'theme') && $kernalSetup->theme == $namespaceTopLevel) {
                    $this->_theme = $namespaceTopLevel;
                }else{
                    $this->_theme = Registry::get('application')['config']->default_theme;
                }
            }
            $this->_parseThemeConfiguration($this->_theme);
        }
    }

    /**
     * Renders all views, blocks within theme & instance
     */
    public function render()
    {
        //when using route->browserRedirect('place') this will prevent rendering and removal of flashMessages.
        if (\Nomad\Core\Registry::get('dispatcher')->getContinueToRenderer() !== true) return;
        if (!$this->_theme) return;
        $themeView = new View(array(
            'file' => StringMethods::makePath(APPLICATION_ROOT, 'theme', $this->_theme, 'layout', strtolower($this->_theme) . '.phtml')
        ));
        //replace namespacing with slashed
        $this->_theme = preg_replace('~\\\~', '/', $this->_theme);

        //generate any dynamic css files.
        $this->_checkCss();

        //create the symlinks for assets. NOTE: YOU MUST CREATE THE THEME FOLDER in /public for this to work.
        $themeAssetDirs = glob(StringMethods::makePath(APPLICATION_ROOT, 'theme', $this->_theme, 'assets', '*'), \GLOB_ONLYDIR);

        foreach ($themeAssetDirs as $dir) {
            $lastPart = substr($dir, strrpos($dir, \DIRECTORY_SEPARATOR) + 1);
			$symDir = StringMethods::makePath(PUBLIC_ROOT, 'assets', $lastPart);

			// copies all assets to the symDir
			// @TODO: merge all css files into {themeName}.css
			$files = glob($dir."/*.*");
			foreach($files as $file){
				if (!file_exists($symDir)) {
					mkdir($symDir, 0755, true);
				}
				$file_to_go = str_replace($dir,$symDir,$file);
				copy($file, $file_to_go);
			}

        }
        //Session::getSession()->aco->isPermitted()

        $themeView->content = $this->_instance->view;//->render();
        foreach ($this->_css as $filename) {
            $themeView->appendCss($filename);
        }

        foreach ($this->_js as $filename) {
            $themeView->appendHeadScript($filename);
        }

        foreach ($this->_blocks as $name => $data)
        {
			/** @var \Nomad\Core\Dispatcher $dispatcher */
            $dispatcher = Registry::get('dispatcher');
            $block = $dispatcher->dispatch(new Simple(array(
                'namespace' => $data['namespace'],
                'action' => $data['action'],
            )));

            if ($block) {
                if (\get_parent_class($block) == 'Nomad\\Form\\AbstractHtmlForm') {
                    $themeView->$name = $block->handle();
                }else{
                    $themeView->$name = $block->view;//->render();
                }
            }
        }
        $themeView = $this->_applyLateBindingAco($themeView, null);
        if (defined('IS_AJAX') && IS_AJAX) {
            echo $this->_instance->view->render();
        }else{
            echo $themeView->render();
        }

    }

    protected function _applyLateBindingAco($view)
    {
        if (Session::getSession()->aco){
            foreach ($view->getData() as $key=>$childView)
            {
                if (is_object($childView) && get_class($childView) == 'Nomad\\Core\\View') {
                    if ($childView->getData()) {
                        $this->_applyLateBindingAco($childView);
                    }

                    if (!Session::getSession()->aco->isPermitted($childView->getCaller())) {
                        $view->$key = '';
                    }
                }
            }
        }
        return $view;
    }


    /**
     * Parse a theme configuration for blocks
     * @param $themeName
     */
    protected function _parseThemeConfiguration($themeName)
    {
        $yamlParser = \Nomad\Configuration\Factory::create('yml');
        $allThemes = $yamlParser->parse(\Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT, 'resource', 'config', 'themes.yml'));
        foreach ($allThemes as $name => $theme) {
            if ($name == $themeName) {
                if (isset($theme->blocks)) {
                    foreach ($theme->blocks as $block) {
                        foreach ($block as $property => $value) {
                            $valueParts = explode('::', $value);
                            $this->_blocks[$property] = ['namespace' => $valueParts[0], 'action' => $valueParts[1]];
                        }
                    }
                }
                if (isset($theme->css)) {
                    foreach ($theme->css as $cssFilename) {
                        $this->_css[] = $cssFilename;
                    }
                }
                if (isset($theme->js)) {
                    foreach ($theme->js as $cssFilename) {
                        $this->_js[] = $cssFilename;
                    }
                }
                return;
            }
        }
    }

    protected function _checkCss()
    {

        $pcssFolder = StringMethods::makePath(APPLICATION_ROOT, 'theme', $this->_theme, 'pcss');
        if (!file_exists($pcssFolder))
            return;

        foreach (glob(StringMethods::makePath($pcssFolder, "*")) as $filename) {
            if (!is_dir($filename)) {
                $firstLine = $rest = "";
                if ($f = fopen($filename, 'r')) {
                    $firstLine = fgets($f); // read until first newline
                    $rest = \stream_get_contents($f);
                    \fclose($f);

                    $meta = \json_decode($firstLine);

                    $newHash = \md5($rest);
                    if (!isset($meta->hash) || $meta->hash != $newHash || (isset($_SERVER['ENVIRONMENT']) && $_SERVER['ENVIRONMENT'] == 'development')) {
                        $this->_rewriteCss($filename, $newHash, $rest);
                    }
                }
            }
        }
    }

    protected function _rewriteCss($pcssfile, $newHash, $body)
    {
        $newMeta = \json_encode(array('hash' => $newHash));
        \file_put_contents($pcssfile, $newMeta . PHP_EOL . $body);

        \ob_start();
        //require_once(StringMethods::makePath(VENDOR_ROOT, 'Nomad', 'src', 'Xcss', 'Color.php'));
        $color = new \Nomad\Xcss\Color();
        $asset = new \Nomad\Xcss\Asset($this->_theme);
        include($pcssfile);
        $css = \ob_get_contents();
        \ob_end_clean();

        $newFilename = preg_replace('~.php$~', '', $pcssfile);
        $newFilepath = preg_replace("~" . $this->_theme . "/pcss~", $this->_theme . '/assets/css', $newFilename);

        if (isset($_SERVER['ENVIRONMENT']) && $_SERVER['ENVIRONMENT'] == 'production') {
            // Remove comments:
            $css = preg_replace('~/\*[^*]*\*+([^/][^*]*\*+)*/~', '', $css);
            // Remove tabs, excessive spaces and newlines
            $css = preg_replace("~(\r\n)|(\r)|(\n)|(\t)~", "", $css);
            // Remove excess spacing
            $css = preg_replace("~[ ]{2,}~", " ", $css);
            // Remove hash from css file
            $css = preg_replace("~\{\".*\":\"[a-f0-9]{32}\"\}~", "", $css);
        }

        \file_put_contents($newFilepath, $css);
    }
}
<?php
namespace Nomad\Validator;
use Nomad\Exception\Form;

abstract class AbstractValidator implements Validator_Interface
{
    /**
     * @var string Message to display when fail
     */
    protected $_message;

    /**
     * @var mixed
     */
    protected $_params;

    /**
     * @var bool Determine to stop processing validators if this fails
     */
    protected $_breakChain = false;

    /**
     * @param array $params OPTIONAL
     * @throws \Nomad\Exception\Form
     */
    public function __construct($params = null)
    {
        foreach ($params as $paramName => $value) {
            $protected = "_" . $paramName;
            if (\property_exists($this, $protected)) {
                $this->$protected = $value;
            }
        }
    }

    /**
     * @param $value
     * @param array $formValues
     * @return mixed
     */
    abstract function isValid($value, $formValues = array());

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->_message;
    }

    /**
     * @return bool
     */
    public function willBreakChain()
    {
        return $this->_breakChain;
    }

}
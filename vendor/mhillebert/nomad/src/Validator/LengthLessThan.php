<?php
namespace Nomad\Validator;

use Nomad\Exception as Exception;
/**
 * LengthGreaterThan.php
 * class to hold the validator
 * @author Mark Hillebert
 * @package The Nomad Project
 */
/**
 * Class Nomad_Validator_LengthGreaterThan
 */
class LengthLessThan extends AbstractValidator
{
    /**
     * @var mixed Value to check against. The validation value.
     */
    protected $_value;

    /**
     * @var string Message to display when validation fails
     */
    protected $_message;

    /**
     * @param array $params
     * @throws \Nomad\Exception\Form
     */
    public function __construct(array $params)
    {
        if (isset($params['value'])){
            if (is_int($params['value'])){
                $this->_value = $params['value'];
            }else{
                throw new Exception\Form('LengthGreaterThan validator "value" must be an integer, ' .
                    gettype($params['value']) . ' given.');
            }
        }else{
            throw new Exception\Form('LengthGreaterThan validator must have "value" in the validator array. e.g: "value"=>5');
        }
        if (!isset($this->_message))
        {
            $this->_message = "Must be less than {$this->_value} characters.";
        }
        parent::__construct($params);
    }

    /**
     * checks for validity.
     * @param string $value
     * @param array $formValues
     * @return bool
     */
    public function isValid($value, $formValues = array())
    {
        if ( strlen($value) < $this->_value  ){

            return true;
        }
        return false;
    }
}
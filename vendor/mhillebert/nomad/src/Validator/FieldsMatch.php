<?php
namespace Nomad\Validator;

use Nomad\Exception as Exception;

/**
 * LengthEqualTo.php
 * class to hold the validator
 * @author Mark Hillebert
 * @package The Nomad Project
 */

/**
 * Class Nomad_Validator_LengthEqualTo
 */
class FieldsMatch extends AbstractValidator
{
    /**
     * @var array
     */
    protected $_fieldsThatMustMatch;

    /**
     * @var string Message to display when validation fails
     */
    protected $_message = "Fields do not match.";

    /**
     * @param array $params
     * @throws \Nomad\Exception\Form_exception
     */
    public function __construct(array $params)
    {
        if (!isset($params['fieldNames'])) {
            throw new Exception\Form_exception('FieldMatch validator "fieldNames" must be passed an array of fields that must match.');
        }

        $this->_fieldsThatMustMatch = $params['fieldNames'];

        parent::__construct($params);
    }

    /**
     * checks for validity.
     * @param string $value
     * @param array $formValues
     * @internal param array|null $params
     * @return bool
     */
    public function isValid($value, $formValues = array())
    {

        foreach ($this->_fieldsThatMustMatch as $fieldName)
        {
            if (!isset($formValues[$fieldName])) {
                return false;
            }

            if ($formValues[$fieldName] != $value) {
                return false;
            }
        }

        return true;
    }
}
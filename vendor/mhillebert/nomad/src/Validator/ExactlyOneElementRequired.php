<?php
namespace Nomad\Validator;

use Nomad\Exception as Exception;

/**
 * LengthEqualTo.php
 * class to hold the validator
 * @author Mark Hillebert
 * @package The Nomad Project
 */

/**
 * Class Nomad_Validator_LengthEqualTo
 */
class ExactlyOneElementRequired extends AbstractValidator
{
    /**
     * @var array
     */
    protected $_fieldsThatMustHaveExactlyOneValue;

    /**
     * @var string Message to display when validation fails
     */
    protected $_message = "At least one must be present.";

    /**
     * @param array $params
     * @throws \Nomad\Exception\Form_exception
     */
    public function __construct(array $params)
    {
        if (!isset($params['fieldNames'])) {
            throw new Exception\Form_exception('FieldMatch validator "fieldNames" must be passed an array of fields that must match.');
        }

        $this->_fieldsThatMustHaveExactlyOneValue = $params['fieldNames'];

        parent::__construct($params);
    }

    /**
     * checks for validity. Only one field in the array may have a value.
     * @param string $value
     * @param array $formValues
     * @internal param array|null $params
     * @return bool
     */
    public function isValid($value, $formValues = array())
    {

        $fieldsSet = 0;
        foreach ($this->_fieldsThatMustHaveExactlyOneValue as $fieldName)
        {
            if (isset($formValues[$fieldName]['error'])) {
                if ($formValues[$fieldName]['error'] != 4) {
                    $fieldsSet++;
                }
            }elseif(isset($formValues[$fieldName])){
                $fieldsSet++;
            }
        }

        return $fieldsSet == 1;
    }
}
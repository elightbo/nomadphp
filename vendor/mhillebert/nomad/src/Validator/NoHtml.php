<?php
namespace Nomad\Validator;


/**
 * Class SanitizedHtml
 * Validates given against digits
 *
 * @package Nomad\Validator
 */
class NoHtml extends AbstractValidator
{
    const NO_HTML = 'no html';
    const DEFAULT_HTML = 'default';
    const FULL_HTML = 'full';

    protected $_filterStrength;

    protected $_allowedTags = array();

    protected $_allowedAttributes = array();


    /**
     * @var string Message to display when validation fails
     */
    protected $_message  = "We are currently unable to accept this.";

    /**
     * Filters valid html tags and no events
     *
     * @param $value
     * @param array $formValues
     * @return bool|mixed
     */
    public function isValid($value, $formValues = array())
    {
        if (empty($value)) return true;


        $config = $this->_getConfiguration();
        $purifier = new \HTMLPurifier($config);
        $filtered = $purifier->purify($value);
        if (strlen($value) == strlen($filtered) ) {

            return true;
        }


        return false;
    }

    /**
     * Sets up the configuration for htmlpurifier
     * @return \HTMLPurifier_Config
     */
    protected function _getConfiguration()
    {
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', '');
        $config->set('HTML.AllowedElements', '');
        $config->set('HTML.AllowedAttributes', '');

        return $config;
    }
}
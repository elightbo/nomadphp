<?php
namespace Nomad\Validator;
/**
 * Class Digit
 * Validates given against digits
 *
 * @package Nomad\Validator
 */
class Digit extends AbstractValidator
{
    public $_message = "Digits only please.";

    /**
     * Checks $value for a non-decimal number (aka integer)
     *
     * @param $value
     * @param array $formValues
     * @return bool|mixed
     */
    public function isValid($value, $formValues = array())
    {
        if (empty($value)) return true;

        return ctype_digit($value);
    }
}
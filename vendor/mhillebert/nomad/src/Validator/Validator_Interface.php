<?php
namespace Nomad\Validator;
/**
 * Interfacer Validator_Interface
 *
 * @package Nomad\Validator
 */
interface Validator_Interface
{
    /**
     * @param      $value
     * @param array $formValues
     * @return mixed
     */
    public function isValid($value, $formValues = array());

    public function getMessage();
}

<?php
namespace Nomad\Validator;
use Nomad\Core\Session;

/**
 * Class Digit
 * Validates given against digits
 *
 * @package Nomad\Validator
 */
class Csrf extends AbstractValidator
{
    const VALID_FORM_DURATION = 21600; //6 hours

    public $_message = "Unable to process the form.";

    /**
     * Checks $value as token set when rendered (set in session)
     *
     * @param $value
     * @param array $formValues
     * @return bool|mixed
     */
    public function isValid($value, $formValues = array())
    {

        /**
         * A wonky way to do this... see the _getSortedKeys in HtmlFormAbstract
         */
        $keys = array_keys($formValues);
        asort($keys);
        $formIdentifier = crc32(implode('', $keys));

        //Get the csrf token from session
        $formCsrf = Session::getSession()->$formIdentifier;

        if ($formCsrf && $formCsrf['token'] === $value && (time() - $formCsrf['timestamp'] < self::VALID_FORM_DURATION)) {
            $return = true;
        }else{
            $return  = false;
        }

        unset(Session::getSession()->$formIdentifier);

        return $return;
    }
}
<?php
namespace Nomad\Validator;


/**
 * Class SanitizedHtml
 * Validates given against digits
 *
 * @package Nomad\Validator
 */
class OnlyNewLineHtml extends AbstractValidator
{
    protected $_filterStrength;

    protected $_allowedTags = array('br');

    protected $_allowedAttributes = array();


    /**
     * @var string Message to display when validation fails
     */
    protected $_message = "We are currently unable to accept this.";

    /**
     * Filters valid html tags and no events
     *
     * @param $value
     * @param array $formValues
     * @return bool|mixed
     */
    public function isValid($value, $formValues = array())
    {
        if (empty($value)) return true;


        $config = $this->_getConfiguration();
        $purifier = new \HTMLPurifier($config);
        $converted = preg_replace("~\r\n~", '', nl2br($value));
        $filtered = $purifier->purify($converted);
        if (strlen($converted) == strlen($filtered)) {

            return true;
        }


        return false;
    }

    /**
     * Sets up the configuration for htmlpurifier
     * @return \HTMLPurifier_Config
     */
    protected function _getConfiguration()
    {
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', '');
        $config->set('HTML.AllowedAttributes', '');
        $config->set('HTML.AllowedElements', implode($this->_allowedTags, ','));

        return $config;
    }
}
<?php
namespace Nomad\Validator;
/**
 * Class Digit
 * Validates given against digits
 *
 * @package Nomad\Validator
 */
class IntegerGreaterThan extends AbstractValidator
{
    /**
     * @var mixed The value the validator is checking against
     */
    protected $_value;

    /**
     * @var string Message to display when fail
     */
    public $_message = "Value was not a digit.";

    /**
     * Checks $value for a non-decimal number (aka integer)
     *
     * @param $value
     * @param array $formValues
     * @return bool|mixed
     */
    public function isValid($value, $formValues = array())
    {
        if (!is_numeric($value)) {
            return false;
        }elseif(strpos($value,'.') !== false){
            $this->_message = 'Please enter a whole number.';
            return false;
        }elseif($value <= $this->_value) {
            $this->_message = "Must be greater than {$this->_value}";
            return false;
        }

        return true;
    }
}
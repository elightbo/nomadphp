<?php
namespace Nomad\Exception;
/**
 * Class Argument
 * Exception class for the autoloader
 *
 * @package Nomad\Exception
 */
class Argument
    extends \Exception
{
    /**
     * @var string Default Error Message
     */
    private static $_message = "Invalid Configuration Driver Type.";

    /**
     * Setterer-upperer
     *
     * @param null $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = NULL, $code = 0, \Exception $previous = NULL)
    {
        if (!$message) {
            $message = self::$_message;
        }
        parent::__construct($message, $code = 0, $previous = NULL);
    }
}
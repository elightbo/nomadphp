<?php
namespace Nomad\Exception;
/**
 * Class PropertyAccess
 * Exception class when a property is invalid or not defined
 *
 * @package Nomad\Exception
 */
class PropertyAccess
    extends \Exception
{
    /**
     * @var string Default Error Message
     */
    private static $_message = "Cannot access property.";

    /**
     * Setterer-upperer
     *
     * @param null $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = NULL, $code = 0, \Exception $previous = NULL)
    {
        if (!$message) {
            $message = self::$_message;
        }
        parent::__construct($message, $code = 0, $previous = NULL);
    }
}
<?php
namespace Nomad\Exception;
/**
 * Class Base_Class
 * Exception class for a base class exception
 *
 * @package Nomad\Exception
 */
class BaseClass
    extends \Exception
{
    /**
     * @var string Default Error Message
     */
    private static $_message = "'You need to call parent::__construct()!";

    /**
     * Setterer-upperer
     *
     * @param null $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = NULL, $code = 0, \Exception $previous = NULL)
    {
        if (!$message) {
            $message = self::$_message;
        }
        parent::__construct($message, $code = 0, $previous = NULL);
    }
}
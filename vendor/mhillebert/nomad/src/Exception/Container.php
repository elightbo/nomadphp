<?php
namespace Nomad\Exception;
/**
 * Class Controller
 * Exception class for the autoloader
 *
 * @package Nomad\Exception
 */
class Container
    extends \Exception
{
    /**
     * @var string Default Error Message
     */
    private static $_message = "Container ERROR.";

    /**
     * Setterer-upperer
     *
     * @param null $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = NULL, $code = 0, \Exception $previous = NULL)
    {
        if (!$message) {
            $message = self::$_message;
        }
        parent::__construct($message, $code = 0, $previous = NULL);
    }
}
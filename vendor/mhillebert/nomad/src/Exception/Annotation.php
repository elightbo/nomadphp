<?php
namespace Nomad\Exception;
/**
 * Class Annotation
 * Exception class for the autoloader
 *
 * @package Nomad\Exception
 */
class Annotation
    extends \Exception
{
    /**
     * @var string Default Error Message
     */
    private static $_message = "You must use a Nomad Implemented Validator,
                        or use one of 'Namespace\\Class::Static(\$value, \$params)' or 'Namespace\\Class->Method(\$value, \$params)' syntax
                        in the @Nomad\\Validator annotation";

    /**
     * Setterer-upperer
     *
     * @param null $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = NULL, $code = 0, \Exception $previous = NULL)
    {
        if (!$message) {
            $message = self::$_message;
        }
        parent::__construct($message, $code = 0, $previous = NULL);
    }
}
<?php
namespace Nomad\Exception;
/**
 * Class MethodIsNotImplemented
 * Exception class for a method that is not implemented
 *
 * @package Nomad\Exception
 */
class Mapper
    extends \Exception
{
    /**
     * @var string Default Error Message
     */
    private static $_message = "Mapper Exception";

    /**
     * Setterer-upperer
     *
     * @param null $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = NULL, $code = 0, \Exception $previous = NULL)
    {
        if (!$message) {
            $message = self::$_message;
        }
        parent::__construct($message, $code = 0, $previous = NULL);
    }
}
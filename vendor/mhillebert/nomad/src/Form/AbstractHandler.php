<?php
namespace Nomad\Form;

    /**
     * Form_HandlerAbstract.php
     * Abstract class for Nomad_Form_Handlers
     * @author Matk Hillebert
     * @package The Nomad Project
     */
/**
 * Class Nomad_Form_HandlerAbstract
 */
abstract class AbstractHandler
{
    public function onSuccess($form)
    {
    }

    public function onFailure($form)
    {
        return $form->render();
    }
}
<?php
namespace Nomad\Form\Element;
    /**
     * Submit.php
     * class to hold the submit element
     * @author Mark Hillebert
     * @package The Nomad Project
     */
/**
 * Class Submit
 * Validates input for digits only.
 */
class Submit extends AbstractElement
{
    protected $_originalValue;

    public function __construct($name, $params = array())
    {
        $this->_originalValue = isset($params['value']) ? $params['value'] : $name;
        parent::__construct($name, $params);
    }

    /**
     * Renders Element
     * @return string
     */
    public function renderElement()
    {
        return "<input type='submit' name='{$this->_name}' {$this->_attributesHtml} {$this->_valueHtml} {$this->_requiredHtml}/>";
    }

    public function isValid($formValuesArray = array())
    {
        if ($this->_originalValue == $this->_value)
            return true;

        $this->_value = $this->_originalValue;
        $this->_errorMessages[] = 'Invalid submit button.';
        return false;
    }
}
<?php
namespace Nomad\Form\Element;
    /**
     * Text.php
     * class to hold the text element
     * @author Mark Hillebert
     * @package The Nomad Project
     */
/**
 * Class Text
 * Validates input for digits only.
 */
class File extends AbstractElement
{
    /**
     * Renders Element
     * @return string
     */
    public function renderElement()
    {
        return "<input type='file' name='{$this->_name}' {$this->_attributesHtml} {$this->_valueHtml} {$this->_requiredHtml}/>";
    }

    public function isValid($formValuesArray = array())
    {
        return true;
    }

}
<?php
namespace Nomad\Form\Element;
    /**
     * Hidden.php
     * class to hold the text element
     * @author Mark Hillebert
     * @package The Nomad Project
     */
/**
 * Class Hidden
 * Validates input for digits only.
 */
class Hidden extends AbstractElement
{
    /**
     * Renders Element
     * @return string
     */
    public function renderElement()
    {
        return "<input type='hidden' name='{$this->_name}' {$this->_attributesHtml} {$this->_valueHtml} {$this->_requiredHtml}/>";
    }

    public function isValid($formValuesArray = array())
    {
        return true;
    }

}
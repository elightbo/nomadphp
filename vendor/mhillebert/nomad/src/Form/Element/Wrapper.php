<?php
namespace Nomad\Form\Element;
    /**
     * Hidden.php
     * class to hold the text element
     * @author Mark Hillebert
     * @package The Nomad Project
     */
/**
 * Class Hidden
 * Validates input for digits only.
 */
class Wrapper extends AbstractElement
{
    public $ignore = true;

    protected $_tag;

    protected $_wrap = array();

    protected $_elements = array();

    /**
     * Renders Element
     * @return string
     */
    public function renderElement()
    {
        $elementsHtmlString = '';
        foreach ($this->_elements as $element)
        {
            $elementsHtmlString .= $element->render(array());
            $this->_isValid &= $element->getValidity();
        }

        return $elementsHtmlString;
    }

    public function isValid($formValuesArray = array())
    {
        return true;
    }

    /**
     * Adds an element object to the array for rendering
     * @param AbstractElement $element
     */
    public function addElement(\Nomad\Form\Element\AbstractElement $element)
    {
        $this->_elements[] = $element;
        $element->rendered = true;
    }

    public function getWrapped()
    {
        return $this->_wrap;
    }

    public function getElements()
    {
        return $this->_elements;
    }
}
<?php
namespace Nomad\Form\Element;
    /**
     * Submit.php
     * class to hold the submit element
     * @author Mark Hillebert
     * @package The Nomad Project
     */
/**
 * Class Submit
 * Validates input for digits only.
 */
class Link extends AbstractElement
{
    protected $_originalValue;

    protected $_href;

    public function __construct($name, $params = array())
    {
        $this->_originalValue = isset($params['value']) ? $params['value'] : $name;
        parent::__construct($name, $params);
    }

    /**
     * Renders Element
     * @return string
     */
    public function renderElement()
    {
        return "<a href='{$this->_href}' {$this->_attributesHtml}>{$this->_value}</a>";
    }

    public function isValid($formValuesArray = array())
    {
        return true;
    }
}
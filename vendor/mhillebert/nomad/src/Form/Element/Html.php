<?php
namespace Nomad\Form\Element;
    /**
     * Hidden.php
     * class to hold the text element
     * @author Mark Hillebert
     * @package The Nomad Project
     */
/**
 * Class Hidden
 * Validates input for digits only.
 */
class Html extends AbstractElement
{
    public $ignore = true;

    /**
     * Renders Element
     * @return string
     */
    public function renderElement()
    {
        return $this->_value;
    }

    public function isValid($formValuesArray = array())
    {
        return true;
    }

}
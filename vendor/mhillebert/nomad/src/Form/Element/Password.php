<?php
namespace Nomad\Form\Element;
    /**
     * Password.php
     * class to hold the password element
     * @author Mark Hillebert
     * @package The Nomad Project
     */
/**
 * Class Password
 * Validates input for digits only.
 */
class Password extends AbstractElement
{
    /**
     * Renders Element
     * @return string
     */
    public function renderElement()
    {
        return "<input type='password' name='{$this->_name}' {$this->_attributesHtml} {$this->_valueHtml} {$this->_requiredHtml}/>";
    }

    public function isValid($formValuesArray = array())
    {
        return true;
    }
}
<?php
namespace Nomad\Form\Element;
    /**
     * Button.php
     * class to hold the button element
     * @author Mark Hillebert
     * @package The Nomad Project
     */
use Nomad\Exception\Form;

/**
 * Class Button
 * Validates input for digits only.
 */
class Button extends AbstractElement
{

    protected $_originalValue;

    public function __construct($name, $params = array())
    {
        $this->_originalValue = isset($params['value']) ? $params['value'] : null;
        parent::__construct($name, $params);
    }

    /**
     * Renders Element
     * @return string
     */
    public function renderElement()
    {
        $this->_valueHtml = !isset($this->_originalValue) ? "" : "value='{$this->_originalValue}'";
        return "<input type='button' name='{$this->_name}' {$this->_attributesHtml} {$this->_valueHtml} {$this->_requiredHtml}/>";
    }

    public function isValid($formValuesArray = array())
    {
        return true;
    }

}
<?php
namespace Nomad\Form\Element;
    /**
     * Textarea.php
     * class to hold the textarea element
     * @author Mark Hillebert
     * @package The Nomad Project
     */
/**
 * Class Textarea
 * Validates input for digits only.
 */
class Textarea extends AbstractElement
{
    /**
     * Renders Element
     * @return string
     */
    public function renderElement()
    {
        return "<textarea name='{$this->_name}' {$this->_attributesHtml} {$this->_requiredHtml}>{$this->_value}</textarea>";
    }

    public function isValid($formValuesArray = array())
    {
        $value = $this->getValue();
        if ($this->_required && empty($value)) {
            $this->_errorMessages[] = "This is required.";
            return false;
        }
        return true;
    }
}
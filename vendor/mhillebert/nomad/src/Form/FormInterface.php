<?php
namespace Nomad\Form;
interface FormInterface
{
    /**
     * Default function to run
     */
    function initialize();

    function createElement($type, $params = array());

    function addSuccessHandler($callable);

    function addFailHandler($callable);

    function removeHandlers($type);

    /**
     * @return mixed Executes the form and performs the handling of submission.
     */
    function handle();

    /**
     * @return mixed Renders the form
     */
    function render();
}
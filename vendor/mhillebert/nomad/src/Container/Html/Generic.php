<?php
namespace Nomad\Container\Html;

class Generic extends AbstractHtmlContainer
{
    protected $_text = '';

    public function setTag($tag)
    {
        $this->_tag = $tag;
        return $this;
    }

}
<?php
namespace Nomad\Container\Html;

class UnorderedList extends Generic
{

    public function __construct($options = array())
    {
        $options['tag'] = 'ul';
        parent::__construct($options);
    }

    /**
     * @param \Nomad\Container\AbstractContainer|array $options
     * @return $this|void
     */
    public function appendChild($options)
    {
        if (is_object($options) && get_parent_class($options) == 'Nomad\Container\Html\AbstractHtmlContainer') {
            $li = $options;
        }else {
            $options['tag'] = 'li';
            $li = new Generic($options);
        }

        parent::appendChild($li);
        return $this;
    }

}
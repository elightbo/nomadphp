<?php
namespace Nomad\Container\AccessControl;

class Role extends \Nomad\Container\Plain
{

    /**
     * @const Only users that are not logged in nor verified.
     */
    const USER_ANONYMOUS = 'anonymous';

    /**
     * @const Any user that has logged in.
     */

    const USER_VERIFIED = 'verified';

    /**
     * Name of role aka Identifier
     * @var string
     */
    protected $_role;

    protected $_asserts;

    public function getRole()
    {
        return $this->_role;
    }
}
<?php
namespace Nomad\Container\AccessControl;

class Resource
{
    /**
     * @var array|\Nomad\Container\AccessControl\Role
     */
    protected $_permittedRoles;

    /**
     * @var array
     */
    protected $_asserts;

    public function __construct($permittedRole = \Nomad\Container\AccessControl\Role::USER_ANONYMOUS, $asserts = array())
    {
        $this->_permittedRoles = $permittedRole;
        if ($asserts) {
            foreach ($asserts as $key => $assert) {
                $this->_asserts[$key] = $asserts;
            }
        }

    }

    public function getPermittedRoles()
    {
        if (is_array($this->_permittedRoles)) {
            return $this->_permittedRoles;
        }

        return (array) $this->_permittedRoles;
    }
}
<?php
namespace Nomad\Xcss;
	/**
	 * Collection of functions to use within a php'd css file
	 */

/**
 * @param $hex
 * @param int $factor
 * @return string
 */
class asset
{
	protected $_theme;

	public function __construct($theme)
	{
		$this->_theme = $theme;
	}

	public function image($image)
	{
		return "url(/assets/images/{$image})";
	}
}

?>
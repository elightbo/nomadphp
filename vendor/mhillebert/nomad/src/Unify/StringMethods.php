<?php
namespace Nomad\Unify;
/**
 * Class StringMethods
 * Static string methods to reduce duplicate coding
 *
 * @package Nomad\Unify
 */
class StringMethods
{
    /**
     * @var string Character for regex to act as delimiter
     */
    protected static $_delimiter = "~";

    /**
     * @return string
     */
    public static function getDelimiter()
    {
        return self::$_delimiter;
    }

    /**
     * @param $delimiter
     */
    public static function setDelimiter($delimiter)
    {
        self::$_delimiter = $delimiter;
    }

    /**
     * Normalizes a pattern to ensure that it is surrounded by only single set of delimiters
     *
     * @param $pattern
     * @return string
     */
    protected static function _normalize($pattern)
    {
        return self::$_delimiter . trim($pattern, self::$_delimiter) . self::$_delimiter;
    }

    /**
     * @param $string
     * @param $pattern
     * @return null
     */
    public static function match($string, $pattern)
    {
        preg_match_all(self::_normalize($pattern), $string, $matches, PREG_PATTERN_ORDER);
        if (!empty($matches[1])) {
            return $matches[1];
        }
        if (!empty($matches[0])) {
            return $matches[0];
        }
        return NULL;
    }

    /**
     * Split a string based on a regex pattern.
     *
     * @param      $string
     * @param      $pattern
     * @param null $limit
     * @return array
     */
    public static function split($string, $pattern, $limit = NULL)
    {
        $flags = PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE;
        return preg_split(self::_normalize($pattern), $string, $limit, $flags);
    }

    /**
     * Takes all passed arguments and joins them with the DIRECTORY SEPARATOR
     *
     * @return string
     */
    public static function makePath()
    {
        $pieces = func_get_args();
        return join(DIRECTORY_SEPARATOR, $pieces);
    }

    public static function makeApplicationPathFromClass($classname, $extension = '.php')
    {
        $parts = explode('\\', $classname);
        return self::makePath(APPLICATION_ROOT, 'package', array_shift($parts), 'src', implode(\DIRECTORY_SEPARATOR, $parts).$extension);
    }

}
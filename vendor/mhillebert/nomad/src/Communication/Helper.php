<?php
namespace Nomad\Communication;
class Helper
{
    public function base64image($filename)
    {
        $type = substr(strrchr($filename,'.'),1);
        switch ($type)
        {
            case 'jpg' : $encodeType = 'jpeg'; break;
            case 'png' : $encodeType = 'png';break;
            default: $encodeType = 'jpeg';
        }
        return "data:image/{$encodeType};base64," . base64_encode(file_get_contents(PUBLIC_ROOT . $filename));
    }


}
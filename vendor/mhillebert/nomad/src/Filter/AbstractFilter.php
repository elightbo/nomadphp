<?php
namespace Nomad\Filter;
abstract class AbstractFilter implements Filter_Interface
{

    protected $_params;

    /**
     * Set any params to their corresponding protected property
     * @param $value
     * @param array $params
     */
    public function __construct($value, $params=array())
    {
        if (!empty($params))
        {
            foreach ($params as $name=>$value)
            {
                $property = "_{$name}";
                if (property_exists($this, $property)) {
                    $this->$property = $value;
                }
            }
        }

    }
    abstract function filter($value, $formValues = array());

}
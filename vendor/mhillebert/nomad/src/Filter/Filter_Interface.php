<?php
namespace Nomad\Filter;
/**
 * Interfacer Validator_Interface
 *
 * @package Nomad\Validator
 */
interface Filter_Interface
{
    /**
     * @param      $value
     * @param array $formValues
     * @return mixed
     */
    public function filter($value, $formValues = array());

}

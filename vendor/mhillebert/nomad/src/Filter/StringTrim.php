<?php
namespace Nomad\Filter;
/**
 * Class StringTrim
 * simply trims string
 *
 * @package Nomad\Filter
 */
class StringTrim extends AbstractFilter
{
    /**
     * Filters valid html tags and no events
     *
     * @param $value
     * @param array $formValues
     * @return bool|mixed
     */
    public function filter($value, $formValues = array())
    {
        return trim($value);
    }
}
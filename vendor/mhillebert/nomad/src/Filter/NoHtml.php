<?php
namespace Nomad\Filter;
/**
 * Class SanitizedHtml
 * Validates given against digits
 *
 * @package Nomad\Validator
 */
class NoHtml extends AbstractFilter
{
    const NO_HTML = 'no html';
    const DEFAULT_HTML = 'default';
    const FULL_HTML = 'full';

    protected $_filterStrength;

    protected $_allowedTags = array(

    );

    protected $_allowedAttributes = array(

    );



    /**
     * Filters valid html tags and no events
     *
     * @param $value
     * @param array $formValues
     * @return bool|mixed
     */
    public function filter($value, $formValues = array())
    {
        $config = $this->_getConfiguration();
        $purifier = new \HTMLPurifier($config);
        //$purifier->
        $filtered = $purifier->purify($value);
        return $filtered;
    }

    /**
     * Sets up the configuration for htmlpurifier
     * @return \HTMLPurifier_Config
     */
    protected function _getConfiguration()
    {
        $config = \HTMLPurifier_Config::createDefault();
        switch ($this->_filterStrength)
        {
            case self::NO_HTML:
                $config->set('HTML.Allowed', '');
                break;
            case self::FULL_HTML:
                //don't modify htmlpurifier's defaults
                break;
            default: //defaults to self::DEFAULT_HTML
                $config->set('HTML.AllowedElements', implode($this->_allowedTags, ','));
                $config->set('HTML.AllowedAttributes', implode($this->_allowedAttributes, ','));
        }

        return $config;
    }
}
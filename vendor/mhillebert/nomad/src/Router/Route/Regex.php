<?php
/**
 * Regex Patterned Route
 */
namespace Nomad\Router\Route;

use Nomad\Router as Router;

/**
 * Class Regex
 * @package Nomad\Core\Router\Route
 */
class Regex extends Router\Route
{
    protected $_keys;

    public function matches($url)
    {
        $pattern = $this->pattern;

        //check values
        preg_match_all("~^{$pattern}$~", $url, $values);

        if (count($values) && count($values[0]) && count($values[1])) {
            //values found, modify params and return
            $derived = array_combine($this->_keys, $values[1]);
            $this->_parameters = array_merge($this->_parameters, $derived);

            return true;
        }

        return false;
    }
}
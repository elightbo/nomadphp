<?php
namespace Application\Controller;

class Index extends \Nomad\Core\Controller
{
	/**
	 * @Nomad\Layout singleColumn
	 */
	public function index()
	{
		$this->view->hello = "Hello";

	}

	/**
	 * @Nomad\Layout singleColumn
	 */
	public function about()
	{
		$this->view->hello = "Hello";

	}

	/**
	 * @Nomad\Layout singleColumn
	 */
	public function docs()
	{
		$this->view->hello = "Hello";

	}

	/**
	 * @Nomad\Layout singleColumn
	 */
	public function showcase()
	{
		$this->view->hello = "Hello";

	}
}
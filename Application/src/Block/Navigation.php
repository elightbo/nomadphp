<?php
namespace Application\Block;

use Nomad\Core\Viewable;

class Navigation extends \Nomad\Core\Controller
{
	public function primary()
	{
		$nav = new \Nomad\Container\Html\Navigation();

		$nav
			->addLink(
				array(
					'label' => 'What is the NomadPhp Framework?',
					'href'  => '/about-nomadphp'
				))
			->addLink(
				array(
					'label' => 'Documentation',
					'href'  => '/docs'
				))
			->addLink(
				array(
					'label' => 'Showcase',
					'href'  => '/showcase'
				));
		$this->view->navigation = $nav->render();
	}
}
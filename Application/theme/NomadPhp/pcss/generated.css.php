{"hash":"17dc767e7c16c82739ab49c5ff4dd0a6"}

<?php
	$mainColor = "#4884E4";
	$secondaryColor = $color->rotate($mainColor, 120);
?>

html{
	background: <?= $asset->image('nice.jpg');?> repeat;
	font-family: Tahoma, Geneva, sans-serif;
}
footer{
	background: linear-gradient(to right, <?= $secondaryColor;?>, <?= $mainColor;?> 33%);
	color: #efefef;
}
header, nav{
	background: linear-gradient(to right, <?= $mainColor;?> 65%, <?= $secondaryColor;?> 95%);
	color: #ffffff;
}